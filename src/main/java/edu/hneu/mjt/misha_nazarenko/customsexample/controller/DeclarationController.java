package edu.hneu.mjt.misha_nazarenko.customsexample.controller;

import edu.hneu.mjt.misha_nazarenko.customsexample.model.Declaration;
import edu.hneu.mjt.misha_nazarenko.customsexample.model.DeclarationDTO;
import edu.hneu.mjt.misha_nazarenko.customsexample.service.DeclarationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Controller
public class DeclarationController {

    @Autowired
    private DeclarationService declarationService;

    @GetMapping("")
    public String showHomePage(){
        return "index";
    }

    @GetMapping("/declarations")
    public String showAllDeclarations(Model model) {
        model.addAttribute("declarations", declarationService.findAll());
        return "showAll";
    }

    @GetMapping("/declarations/new")
    public String showCreateForm(Model model) {
        model.addAttribute("declaration", new DeclarationDTO());
        return "createDeclaration";
    }

    @PostMapping("/declarations")
    public String createDeclaration(@ModelAttribute("declaration") DeclarationDTO declarationDTO, Model model) {
        String validationError = validateDeclaration(declarationDTO);
        if (validationError != null) {
            model.addAttribute("errorMessage", validationError);
            return "createDeclaration";
        }
        Declaration declaration = declarationDTO.toEntity();
        declarationService.save(declaration);
        return "redirect:/declarations";
    }

    @GetMapping("/declarations/edit/{id}")
    public String showEditForm(@PathVariable("id") Integer id, Model model) {
        Declaration declaration = declarationService.findById(id);
        model.addAttribute("declaration", DeclarationDTO.fromEntity(declaration));
        return "editDeclaration";
    }

    @PostMapping("/declarations/{id}")
    public String updateDeclaration(@PathVariable("id") Integer id, @ModelAttribute("declaration") DeclarationDTO declarationDTO, Model model) {
        String validationError = validateDeclaration(declarationDTO);
        if (validationError != null) {
            model.addAttribute("errorMessage", validationError);
            return "editDeclaration";
        }
        Declaration declaration = declarationDTO.toEntity();
        declaration.setId(id);
        declarationService.save(declaration);
        return "redirect:/declarations";
    }

    @GetMapping("/declarations/delete/{id}")
    public String deleteDeclaration(@PathVariable("id") Integer id) {
        declarationService.deleteById(id);
        return "redirect:/declarations";
    }

    private String validateDeclaration(DeclarationDTO declarationDTO) {
        // Перевірка обов'язкових полів
        if (declarationDTO.getDate() == null || declarationDTO.getDate().isEmpty()
                || declarationDTO.getLastName() == null || declarationDTO.getLastName().isEmpty()
                || declarationDTO.getFirstName() == null || declarationDTO.getFirstName().isEmpty()
                || declarationDTO.getSurname() == null || declarationDTO.getSurname().isEmpty()
                || declarationDTO.getCitizenship() == null || declarationDTO.getCitizenship().isEmpty()
                || declarationDTO.getCountryOfPermanentResidence() == null || declarationDTO.getCountryOfPermanentResidence().isEmpty()
                || declarationDTO.getPassportNumber() == null || declarationDTO.getPassportNumber().isEmpty()
                || declarationDTO.getPassportSeries() == null || declarationDTO.getPassportSeries().isEmpty()
                || declarationDTO.getCountryOfDeparture() == null || declarationDTO.getCountryOfDeparture().isEmpty()
                || declarationDTO.getDeclaredAmount() == null || Double.isNaN(declarationDTO.getDeclaredAmount())
                || declarationDTO.getCurrencyCode() == null || declarationDTO.getCurrencyCode().isEmpty()
                || declarationDTO.getInformationOnTheAvailabilityOfLuggage() == null || declarationDTO.getInformationOnTheAvailabilityOfLuggage().isEmpty()) {
            return "Будь ласка, заповніть усі обов'язкові поля.";
        }

        // Перевірка формату дати
        if (declarationDTO.getDate().isEmpty()) {
            return "Дата є обов'язковою.";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateFormat.parse(declarationDTO.getDate());
        } catch (ParseException e) {
            return "Невірний формат дати. Використовуйте формат yyyy-MM-dd.";
        }

        // Перевірка формату суми декларації
        if (declarationDTO.getDeclaredAmount() <= 0) {
            return "Задекларована сума повинна бути позитивною.";
        }

        // Перевірка на валідність номера паспорта
        if (!declarationDTO.getPassportNumber().matches("^[A-Z0-9]{6,12}$")) {
            return "Номер паспорта повинен містити від 6 до 12 алфавітно-цифрових символів.";
        }

        // Перевірка на валідність серії паспорта
        if (!declarationDTO.getPassportSeries().matches("^[A-Z0-9]{2,4}$")) {
            return "Серія паспорта повинна містити від 2 до 4 алфавітно-цифрових символів.";
        }

        // Перевірка на валідність коду валюти
        if (!declarationDTO.getCurrencyCode().matches("^[A-Z]{3}$")) {
            return "Код валюти повинен містити 3 великі літери.";
        }

        return null;
    }
}
