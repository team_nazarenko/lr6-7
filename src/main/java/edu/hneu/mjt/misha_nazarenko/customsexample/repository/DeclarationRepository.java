package edu.hneu.mjt.misha_nazarenko.customsexample.repository;

import edu.hneu.mjt.misha_nazarenko.customsexample.model.Declaration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeclarationRepository extends CrudRepository<Declaration, Integer> {
}