package edu.hneu.mjt.misha_nazarenko.customsexample.model;

import lombok.Data;

@Data
public class DeclarationDTO {
    private Integer id;
    private String date;
    private String lastName;
    private String firstName;
    private String surname;
    private String citizenship;
    private String countryOfPermanentResidence;
    private String passportNumber;
    private String passportSeries;
    private String countryOfDeparture;
    private Double declaredAmount;
    private String currencyCode;
    private String informationOnTheAvailabilityOfLuggage;
    public static DeclarationDTO fromEntity(Declaration entity) {
        DeclarationDTO dto = new DeclarationDTO();
        dto.setId(entity.getId());
        dto.setDate(entity.getDate());
        dto.setLastName(entity.getLastName());
        dto.setFirstName(entity.getFirstName());
        dto.setSurname(entity.getSurname());
        dto.setCitizenship(entity.getCitizenship());
        dto.setCountryOfPermanentResidence(entity.getCountryOfPermanentResidence());
        dto.setPassportNumber(entity.getPassportNumber());
        dto.setPassportSeries(entity.getPassportSeries());
        dto.setCountryOfDeparture(entity.getCountryOfDeparture());
        dto.setDeclaredAmount(entity.getDeclaredAmount());
        dto.setCurrencyCode(entity.getCurrencyCode());
        dto.setInformationOnTheAvailabilityOfLuggage(entity.getInformationOnTheAvailabilityOfLuggage());
        return dto;
    }

    public Declaration toEntity() {
        Declaration entity = new Declaration();
        entity.setId(this.getId());
        entity.setDate(this.getDate());
        entity.setLastName(this.getLastName());
        entity.setFirstName(this.getFirstName());
        entity.setSurname(this.getSurname());
        entity.setCitizenship(this.getCitizenship());
        entity.setCountryOfPermanentResidence(this.getCountryOfPermanentResidence());
        entity.setPassportNumber(this.getPassportNumber());
        entity.setPassportSeries(this.getPassportSeries());
        entity.setCountryOfDeparture(this.getCountryOfDeparture());
        entity.setDeclaredAmount(this.getDeclaredAmount());
        entity.setCurrencyCode(this.getCurrencyCode());
        entity.setInformationOnTheAvailabilityOfLuggage(this.getInformationOnTheAvailabilityOfLuggage());
        return entity;
    }
}
