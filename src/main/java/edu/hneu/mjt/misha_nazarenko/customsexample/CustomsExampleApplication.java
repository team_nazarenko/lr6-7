package edu.hneu.mjt.misha_nazarenko.customsexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomsExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomsExampleApplication.class, args);
    }

}
