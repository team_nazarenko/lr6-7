package edu.hneu.mjt.misha_nazarenko.customsexample.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "declaration")
public class Declaration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "date", nullable = false)
    private String date;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "citizenship", nullable = false)
    private String citizenship;

    @Column(name = "country_of_permanent_residence", nullable = false)
    private String countryOfPermanentResidence;

    @Column(name = "passport_number", nullable = false)
    private String passportNumber;

    @Column(name = "passport_series", nullable = false)
    private String passportSeries;

    @Column(name = "country_of_departure", nullable = false)
    private String countryOfDeparture;

    @Column(name = "declared_amount", nullable = false)
    private Double declaredAmount;

    @Column(name = "currency_code", nullable = false)
    private String currencyCode;

    @Column(name = "information_on_the_availability_of_luggage", nullable = false)
    private String informationOnTheAvailabilityOfLuggage;
}
