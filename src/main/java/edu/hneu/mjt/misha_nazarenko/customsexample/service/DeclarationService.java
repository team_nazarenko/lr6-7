package edu.hneu.mjt.misha_nazarenko.customsexample.service;

import edu.hneu.mjt.misha_nazarenko.customsexample.model.Declaration;
import edu.hneu.mjt.misha_nazarenko.customsexample.repository.DeclarationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DeclarationService {

    @Autowired
    private DeclarationRepository declarationRepository;

    public List<Declaration> findAll() {
        return StreamSupport.stream(declarationRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Declaration findById(Integer id) {
        return declarationRepository.findById(id).orElse(null);
    }

    public void save(Declaration declaration) {
        declarationRepository.save(declaration);
    }

    public void deleteById(Integer id) {
        declarationRepository.deleteById(id);
    }
}
