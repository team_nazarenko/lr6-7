package edu.hneu.mjt.misha_nazarenko.customsexample;

import edu.hneu.mjt.misha_nazarenko.customsexample.model.Declaration;
import edu.hneu.mjt.misha_nazarenko.customsexample.repository.DeclarationRepository;
import edu.hneu.mjt.misha_nazarenko.customsexample.service.DeclarationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DeclarationServiceTest {

    @Mock
    private DeclarationRepository declarationRepository;

    @InjectMocks
    private DeclarationService declarationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testFindAll() {
        Declaration declaration1 = new Declaration();
        Declaration declaration2 = new Declaration();
        when(declarationRepository.findAll()).thenReturn(Arrays.asList(declaration1, declaration2));

        List<Declaration> declarations = declarationService.findAll();

        assertNotNull(declarations);
        assertEquals(2, declarations.size());
        verify(declarationRepository, times(1)).findAll();
    }

    @Test
    void testFindById() {
        Declaration declaration = new Declaration();
        declaration.setId(1);
        when(declarationRepository.findById(1)).thenReturn(Optional.of(declaration));

        Declaration foundDeclaration = declarationService.findById(1);

        assertNotNull(foundDeclaration);
        assertEquals(1, foundDeclaration.getId());
        verify(declarationRepository, times(1)).findById(1);
    }

    @Test
    void testSave() {
        Declaration declaration = new Declaration();
        declarationService.save(declaration);

        verify(declarationRepository, times(1)).save(declaration);
    }

    @Test
    void testDeleteById() {
        declarationService.deleteById(1);

        verify(declarationRepository, times(1)).deleteById(1);
    }
}

